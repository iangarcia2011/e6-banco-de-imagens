from flask import Flask, request, jsonify, safe_join
from environs import Env
from time import sleep
from os import popen
import os
import werkzeug

from werkzeug.utils import secure_filename

env = Env()
env.read_env()

file_folder = os.environ.get('FILES_DIRECTORY')   



def link_file(type:str, file_name:str='') -> str:
    """
    NOTE: Receba o tipo e opcionalmente o nome do arquivo,
        então returna a rota do arquivo.
    """
    return f'{file_folder}/{type}/{file_name}'


def create_zip_file(type:str, compression:str='1')->str:
    """
    NOTE: Recebe o tipo do arquivo e o nivel de compressão  
        e cria um arquivo zip com todos os arquivos do tipo. 
    """
    
    route_zip = f"{file_folder}/arg_zip.zip"
    if os.path.exists(route_zip):

        os.remove(route_zip)

    os.system(f'zip -{compression} {route_zip} {f"{file_folder}/{type}/*"}')

    return None


def type_file(filename:str) ->str:
    """ 
        NOTE: Pega o nome do arquivo e retorna uma str com seu tipo.
    """
    
    type = ''
    for i, l in enumerate(filename):
        if l == '.':
            type = filename[i+1:]
    return type

    
def save_files(filename:str, received_file, respost:dict)->dict :
    """
    (filename: str, received_file: FileStorage, respost: dict) -> dict

    NOTE: Salva os arquivos separados por tipo na pasta 'files'.
    """

    type = type_file(filename)

    accepted_types = ['gif','png','jpg']
    for typ in accepted_types:

        if type == typ:

            if not os.path.exists(link_file(type)):
                os.makedirs(link_file(type))

    if os.path.exists(link_file(type)):

        if not os.path.exists(link_file(type,filename)):
        
            file_path = safe_join(link_file(type), filename)
            
            received_file.save(file_path) 

            respost['msg'], respost['status']  = 'successful upload', 201                   
        else:
            respost['msg'], respost['status']  = f'arquifo {filename} já existente', 409  

    return respost 



