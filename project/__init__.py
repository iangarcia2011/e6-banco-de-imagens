from flask import Flask, request, jsonify, safe_join, send_from_directory
from environs import Env
import os

from werkzeug.utils import secure_filename

from kenzie.image import type_file, link_file, create_zip_file, save_files

env = Env()
env.read_env()

file_folder = os.environ.get('FILES_DIRECTORY')   # > env
max_con_len = int(os.environ.get('MAX_CONTENT_LENGTH'))  

# MAX_CONTENT_LENGTH
app = Flask(__name__)


if not os.path.exists(file_folder):
    os.makedirs(file_folder)



@app.get('/download-zip')
def download_dir_as_zip():
    
    type = request.args['file_type']

    if not os.path.exists(link_file(type)):
        return {'msg': 'Não temos arquivos deste tipo'}, 404

    if len(request.args) > 1:
        compression =  request.args['compression_rate']
        create_zip_file(type, compression)
      
    create_zip_file(type)

    arg_zip = 'arg_zip.zip'

    return send_from_directory(f'.{file_folder}', arg_zip,  as_attachment= arg_zip ), 200



@app.get('/download/<file_name>')
def download(file_name):   
    
    type = type_file(file_name)

    if os.path.exists(link_file(type,file_name)):
        
        return send_from_directory(f'.{link_file(type)}', file_name,  as_attachment= file_name ), 200

    else:

        return {'msg': 'não temos este arquivo'}, 404
    


@app.get('/files')
def list_files():
    lista_files = []

    for _,_, filenames in os.walk(f"{file_folder}"):
        lista_files.extend(filenames)

    return jsonify(lista_files)

    
@app.get('/files/<type>')
def list_files_by_type(type):
    try:

        files = os.listdir(link_file(type))

        return jsonify(files)
    except FileNotFoundError:
        return {'msg': 'Não extite nenhum arquivo com este tipo'}, 404


    
@app.post('/upload')
def upload():
    files_list = list(request.files)
    file_size_ = request.headers['Content-Length']

    respost = {
        'msg': '',
        'status': 000
    }
     
    if int(file_size_) > max_con_len:
        respost['msg'], respost['status']  = 'arquivo com tamanho maior que o permitido', 413
        return respost, respost['status']

    if not files_list == []:

        for f in files_list:

            received_file = request.files[f]
            print('\ntype>>>',type(received_file))
            filename = secure_filename(received_file.filename)

            save_files(filename, received_file , respost)
                  
    else:
        respost['msg'], respost['status']  = 'Coloque um arquivo', 404

    if respost['msg'] == '':
        respost['msg'], respost['status']  = f"O '{filename}' não é um arquivo com formato falido" , 415

    
    return respost, respost['status']

    